from django.db import models
from decimal import Decimal


class Constructor(models.Model):
    constructorId = models.CharField(max_length=20, unique=True, primary_key=True)
    url = models.URLField()
    name = models.CharField(max_length=20)
    nationality = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Driver(models.Model):
    driverId = models.CharField(max_length=20, unique=True, primary_key=True)
    permanentNumber = models.CharField(max_length=3, unique=True)
    code = models.CharField(max_length=3, unique=True)
    wiki_url = models.URLField()
    givenName = models.CharField(max_length=20)
    familyName = models.CharField(max_length=20)
    dateOfBirth = models.CharField(max_length=10)
    nationality = models.CharField(max_length=20)
    current_constructor = models.ForeignKey(Constructor, related_name='current_constructor', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.givenName} {self.familyName}"


class DriverStandings(models.Model):
    position = models.IntegerField(unique=True, primary_key=True)
    driver = models.OneToOneField(Driver, on_delete=models.CASCADE, related_name='drivers', unique=True)
    wins = models.IntegerField()
    points = models.DecimalField(decimal_places=1, max_digits=5, default=Decimal(0.0))

    def __str__(self):
        return self.driver
