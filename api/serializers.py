from rest_framework import serializers

from .models import DriverStandings, Driver, Constructor


class ConstructorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Constructor
        fields = '__all__'


class DriverSerializer(serializers.ModelSerializer):
    current_constructor = ConstructorSerializer(read_only=False)

    class Meta:
        model = Driver
        fields = '__all__'


class DriverStandingsSerializer(serializers.ModelSerializer):
    driver = DriverSerializer(read_only=False)

    class Meta:
        model = DriverStandings
        fields = '__all__'

