from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('constructors', views.ConstructorView)
router.register('drivers', views.DriverView)
router.register('driver-standings', views.DriverStandingsView)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls))
]


